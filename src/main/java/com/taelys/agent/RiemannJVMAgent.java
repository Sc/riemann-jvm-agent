package com.taelys.agent;

import java.lang.instrument.Instrumentation;

import com.taelys.agent.collect.Collector;
import com.taelys.agent.collect.Listener;
import com.taelys.agent.configuration.Configuration;
import com.taelys.agent.output.Outputter;

public class RiemannJVMAgent {

    public static void premain(String confFile, Instrumentation instrumentation) throws Exception {
        System.out.println("RiemannJVMAgent launched with configuration [ " + confFile + " ]");

        Configuration conf = Configuration.build(confFile);
        Outputter outputter = Outputter.outputter(conf);

        Listener.startListening(conf, outputter);
        Collector.startCollecting(conf, outputter);
    }
}
