package com.taelys.agent.collect;

import java.lang.management.ManagementFactory;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;

import com.taelys.agent.configuration.Configuration;
import com.taelys.agent.configuration.Query;
import com.taelys.agent.output.Event;
import com.taelys.agent.output.Outputter;

public class Collector {

    private static MBeanServer mbeanServer = ManagementFactory.getPlatformMBeanServer();

    public static void startCollecting(final Configuration conf, final Outputter outputter) {
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    collect(conf.getQueries().values(), outputter);
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }
        }, 0, conf.getCollectInterval(), TimeUnit.SECONDS);
    }

    private static void collect(Collection<Query> queries, Outputter outputter) {
        for (Query query : queries) {
            try {
                Object attributeValue = mbeanServer.getAttribute(new ObjectName(query.getObject()), query.getAttribute());
                if (attributeValue instanceof CompositeData) {
                    CompositeData compositeData = (CompositeData) attributeValue;
                    attributeValue = compositeData.get(query.getKey());
                }
                outputter.write(new Event(query.getAlias(), attributeValue == null ? null : attributeValue.toString()));
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
        outputter.flush();
    }

}
