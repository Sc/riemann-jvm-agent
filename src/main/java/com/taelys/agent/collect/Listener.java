package com.taelys.agent.collect;

import java.lang.management.ManagementFactory;

import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeDataSupport;

import com.taelys.agent.configuration.Configuration;
import com.taelys.agent.configuration.Notification;
import com.taelys.agent.output.Event;
import com.taelys.agent.output.Outputter;

public class Listener {

    public static void startListening(Configuration conf, final Outputter outputter) {
        for (final Notification notification : conf.getNotifications().values()) {
            try {
                ManagementFactory.getPlatformMBeanServer().addNotificationListener(new ObjectName(notification.getObject()), new NotificationListener() {
                    @Override
                    public void handleNotification(javax.management.Notification notificationEvent, Object handback) {
                        CompositeDataSupport userData = (CompositeDataSupport) notificationEvent.getUserData();
                        CompositeDataSupport data = (CompositeDataSupport) userData.get(notification.getData());
                        Object attributeValue = data.get(notification.getAttribute());
                        outputter.write(new Event(notification.getAlias(), attributeValue.toString()));
                    }
                }, null, null);
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
    }

}
