package com.taelys.agent.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class Configuration {

    static final String OUTPUT = "output";
    static final String RIEMANN_HOST = "riemann.host";
    static final String RIEMANN_PORT = "riemann.port";
    static final String COLLECT_HOST = "collect.host";
    static final String COLLECT_INTERVAL = "collect.interval";

    static final String QUERY_PREFIX = "query";
    static final String QUERY_OBJECT_SUFFIX = "object";
    static final String QUERY_ATTRIBUTE_SUFFIX = "attribute";
    static final String QUERY_KEY_SUFFIX = "key";
    static final String QUERY_ALIAS_SUFFIX = "alias";

    static final String NOTIFICATION_PREFIX = "notification";
    static final String NOTIFICATION_OBJECT_SUFFIX = "object";
    static final String NOTIFICATION_DATA_SUFFIX = "data";
    static final String NOTIFICATION_ATTRIBUTE_SUFFIX = "attribute";
    static final String NOTIFICATION_ALIAS_SUFFIX = "alias";

    private String output;
    private String riemannHost;
    private int riemannPort;
    private String host;
    private int collectInterval;
    private Map<String, Query> queries = new HashMap<>();
    private Map<String, Notification> notifications = new HashMap<>();

    public static Configuration build(String confFile) throws IOException {
        try (InputStream inputStream = new FileInputStream(confFile)) {
            return new Configuration(inputStream);
        }
    }

    private Configuration(InputStream inputStream) throws IOException {
        Properties prop = new Properties();
        prop.load(inputStream);

        for (Entry<Object, Object> entry : prop.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            if (key.startsWith(QUERY_PREFIX)) {
                String[] parts = key.split("\\.");
                String queryName = parts[parts.length-3]+parts[parts.length-2];
                String queryProperty = parts[parts.length-1];

                Query query = this.queries.get(queryName);
                if (query == null) {
                    query = new Query();
                    this.queries.put(queryName, query);
                }

                switch (queryProperty) {
                    case QUERY_OBJECT_SUFFIX:
                        query.object = value;
                        break;
                    case QUERY_ATTRIBUTE_SUFFIX:
                        query.attribute = value;
                        break;
                    case QUERY_KEY_SUFFIX:
                        query.key = value;
                        break;
                    case QUERY_ALIAS_SUFFIX:
                        query.alias = value;
                        break;
                    default:
                        throw new RuntimeException("Unknown qyery property " + key);
                }
            } else if (key.startsWith(NOTIFICATION_PREFIX)) {
                String[] parts = key.split("\\.");
                String notificationName = parts[parts.length-3]+parts[parts.length-2];
                String notificationProperty = parts[parts.length-1];

                Notification notification = this.notifications.get(notificationName);
                if (notification == null) {
                    notification = new Notification();
                    this.notifications.put(notificationName, notification);
                }

                switch (notificationProperty) {
                    case NOTIFICATION_OBJECT_SUFFIX:
                        notification.object = value;
                        break;
                    case NOTIFICATION_DATA_SUFFIX:
                        notification.data = value;
                        break;
                    case NOTIFICATION_ATTRIBUTE_SUFFIX:
                        notification.attribute = value;
                        break;
                    case NOTIFICATION_ALIAS_SUFFIX:
                        notification.alias = value;
                        break;
                    default:
                        throw new RuntimeException("Unknown notification property " + key);
                }
            } else {
                switch (key) {
                    case OUTPUT:
                        this.output = value;
                        break;
                    case RIEMANN_HOST:
                        this.riemannHost = value;
                        break;
                    case RIEMANN_PORT:
                        this.riemannPort = Integer.valueOf(value);
                        break;
                    case COLLECT_HOST:
                        this.host = value;
                        break;
                    case COLLECT_INTERVAL:
                        this.collectInterval = Integer.valueOf(value);
                        break;
                    default:
                        throw new RuntimeException("Unknown general property " + key);
                }
            }

        }
    }

    public String getOutput() {
        return this.output;
    }

    public String getRiemannHost() {
        return this.riemannHost;
    }

    public int getRiemannPort() {
        return this.riemannPort;
    }

    public String getHost() {
        return this.host;
    }

    public int getCollectInterval() {
        return this.collectInterval;
    }

    public Map<String, Query> getQueries() {
        return this.queries;
    }

    public Map<String, Notification> getNotifications() {
        return this.notifications;
    }

    public String toString() {
        return "Configuration(host=" + this.getHost() + ", riemannHost=" + this.getRiemannHost() + ", riemannPort=" + this.getRiemannPort() + ", collectInterval=" + this.getCollectInterval() + ", queries=" + this.getQueries() + ")";
    }
}
