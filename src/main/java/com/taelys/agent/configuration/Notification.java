package com.taelys.agent.configuration;

public class Notification {

    String object;
    String data;
    String attribute;
    String alias;

    public String getObject() {
        return this.object;
    }

    public String getData() {
        return this.data;
    }

    public String getAttribute() {
        return this.attribute;
    }

    public String getAlias() {
        return this.alias;
    }

    public String toString() {
        return "Notification(object=" + this.getObject() + ", data=" + this.getData() + ", attribute=" + this.getAttribute() + ", alias=" + this.getAlias() + ")";
    }
}
