package com.taelys.agent.configuration;

public class Query {

    String object;
    String attribute;
    String key;
    String alias;

    public String getObject() {
        return this.object;
    }

    public String getAttribute() {
        return this.attribute;
    }

    public String getKey() {
        return this.key;
    }

    public String getAlias() {
        return this.alias;
    }

    public String toString() {
        return "Query(object=" + this.getObject() + ", attribute=" + this.getAttribute() + ", key=" + this.getKey() + ", alias=" + this.getAlias() + ")";
    }
}
