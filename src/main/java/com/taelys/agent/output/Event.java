package com.taelys.agent.output;

public class Event {

    private String service;
    private String value;

    public Event(String service, String value) {
        this.service = service;
        this.value = value;
    }

    String getService() {
        return this.service;
    }

    String getValue() {
        return this.value;
    }
}
