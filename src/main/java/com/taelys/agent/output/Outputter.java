package com.taelys.agent.output;

import com.taelys.agent.configuration.Configuration;

public abstract class Outputter {

    String RIEMANN_JVM_AGENT = "riemann-jvm-agent";

    public static Outputter outputter(Configuration conf) throws Exception {
        Outputter outputter;

        String output = conf.getOutput();
        switch (output) {
            case "StdOutput":
                outputter = new StdOutputter(conf.getHost());
                break;
            case "RiemannWs":
                outputter = new RiemannWsOutputter(conf.getRiemannHost(), conf.getRiemannPort(), conf.getHost());
                break;
            default:
                throw new RuntimeException("Unknown output " + output);
        }

        return outputter;
    }

    public abstract void write(Event var1);

    public abstract void flush();

}
