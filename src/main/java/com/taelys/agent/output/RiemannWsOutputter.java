package com.taelys.agent.output;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedList;

public class RiemannWsOutputter extends Outputter {

    private URL riemannUrl;
    private String host;

    private Collection<Event> events = new LinkedList<>();

    public RiemannWsOutputter(String riemannHost, int riemannPort, String host) throws Exception {
        this.riemannUrl = new URL("http://" + riemannHost + ":" + riemannPort + "/events");
        this.host = host;
    }

    public void write(Event event) {
        this.events.add(event);
    }

    public void flush() {
        Collection<Event> sentEvents = this.events;
        this.events = new LinkedList<>();

        try {
            HttpURLConnection con = (HttpURLConnection) this.riemannUrl.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);

            try (OutputStream os = con.getOutputStream()) {
                StringBuilder body = new StringBuilder();
                for (Event sentEvent : sentEvents) {
                    body.append("{");
                    body.append("\"tags\": [\"").append(RIEMANN_JVM_AGENT).append("\"]");
                    body.append(",");
                    body.append("\"host\": \"").append(this.host).append("\"");
                    body.append(",");
                    body.append("\"service\": \"").append(sentEvent.getService()).append("\"");
                    body.append(",");
                    body.append("\"metric\": ").append(sentEvent.getValue());
                    body.append("}\n");
                }

                os.write(body.toString().getBytes(StandardCharsets.UTF_8));
                os.flush();
            }
            int responseCode = con.getResponseCode();
            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
