package com.taelys.agent.output;

public class StdOutputter extends Outputter {

    private String host;

    public StdOutputter(String host) {
        this.host = host;
    }

    public void write(Event event) {
        System.out.println(event.getService() + "@" + this.host + " : " + event.getValue());
    }

    public void flush() {
        // Nothing to do
    }

}
