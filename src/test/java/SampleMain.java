import com.taelys.agent.RiemannJVMAgent;

public class SampleMain {

    public static void main(String[] args) throws Exception {
        // Sample test configuration
        RiemannJVMAgent.premain("src/test/resources/riemann-jvm-agent-sample-conf.properties", null);

        // Some allocations to fire GCs
        for (int i = 0; i < 100; i++) {
            byte[] bytes = new byte[3000000];
            Thread.sleep(100L);
        }
    }

}
